<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Softdrink;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class SodaAdminController extends Controller
{
    /**
     * @Route("/admin", name="administration")
     */
    public function indexAction(Request $request)
    //regular controller for the admin-interface.
    {
        //Get all products from database
        $products = $this->getDoctrine()->getRepository('AppBundle:Softdrink');
        $products = $products->findAll();
        //Render template with all products from database.
        return $this->render('admin.html.twig', array(
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..'),
            'products' => $products,
            )
        );
    }
    /**
     * @Route("/admin/order", name="order")
     */
    public function ordersodaAction(Request $request)
    //Controller for placing an order and updating stock.
    {
        if($this->getRequest()->isMethod('POST')){
            $orderid = $request->request->get('id');
            $orderamount = $request->request->get('amount');
            $em = $this->getDoctrine()->getManager();

            $name = $em->getRepository('AppBundle:Softdrink')->find($orderid);
            $name = $name->getName();
            $price = $em->getRepository('AppBundle:Softdrink')->find($orderid);
            $price = $price->getPrice();
            $ordertotalprice = $price*$orderamount;

            $updateStatus = $this->reducestockAction($orderid, $orderamount);

            $products = $this->getDoctrine()->getRepository('AppBundle:Softdrink');
            $products = $products->findAll();



            if($updateStatus != "error"){
                return $this->render('admin.html.twig', array(
                    'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..'),
                    'info' => $orderamount.' '.$name.' was ordered at a total price of: '.$ordertotalprice.' DKK',
                    'products' => $products,
                    'orderid' => $orderid,
                    'orderamount' => $orderamount,
                ));
            }
            else{
                return $this->render('admin.html.twig', array(
                    'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..'),
                    'alert' =>'unable to create order (Check values)!',
                    'products' => $products,
                ));
            }
        }
        else{
            return $this->redirectToRoute('administration', array());
        }
    }

    public function reducestockAction($id, $amount){
        $em = $this->getDoctrine()->getManager();
        $product = $em->getRepository('AppBundle:Softdrink')->find($id);

        if (!$product || ($product->getStock() < $amount || $amount <= 0)) {
            return "error";
        }

        $product->setStock($product->getStock() - $amount);
        $em->flush();
    }

    /**
     * @Route("/admin/updatesoda", name="updatesoda")
     */
    public function updatesodaAction(Request $request){
        if($this->getRequest()->isMethod('POST')) {
            $sodaID = $request->request->get('id');
            $price = $request->request->get('price');
            $stock = $request->request->get('stock');
            $name = $request->request->get('name');
            $description = $request->request->get('description');

            $em = $this->getDoctrine()->getManager();
            $product = $em->getRepository('AppBundle:Softdrink')->find($sodaID);

            $product->SetStock($stock);
            $product->SetName($name);
            $product->SetDescription($description);
            $product->SetPrice($price);

            $em->flush();

            //Get all products from database
            $products = $this->getDoctrine()->getRepository('AppBundle:Softdrink');
            $products = $products->findAll();
            //Render template with all products from database.
            return $this->render('admin.html.twig', array(
                    'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..'),
                    'products' => $products,
                    'info' => $name.' was successfully updated!',
                )
            );
        }
        else{
            return $this->redirectToRoute('administration', array());
        }
    }
    /**
     * @Route("/admin/deletesoda", name="deletesoda")
     */
    public function deletesodaAction(Request $request){
        if($this->getRequest()->isMethod('POST')) {
            $sodaID = $request->request->get('id');


            $em = $this->getDoctrine()->getManager();
            $product = $em->getRepository('AppBundle:Softdrink')->find($sodaID);

            $em = $this->getDoctrine()->getManager();
            $name = $em->getRepository('AppBundle:Softdrink')->find($sodaID);
            $name = $name->getName();

            $em->remove($product);
            $em->flush();

            //Get all products from database
            $products = $this->getDoctrine()->getRepository('AppBundle:Softdrink');
            $products = $products->findAll();
            //Render template with all products from database.
            return $this->render('admin.html.twig', array(
                    'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..'),
                    'products' => $products,
                    'info' => $name.' was deleted!',
                )
            );
        }
        else{
            return $this->redirectToRoute('administration', array());
        }
    }

    /**
     * @Route("/admin/addsoda", name="addsoda")
     */
    public function addsodaAction(Request $request){
        if($this->getRequest()->isMethod('POST')) {
            $price = $request->request->get('price');
            $stock = $request->request->get('stock');
            $name = $request->request->get('name');
            $description = $request->request->get('description');

            $product = new Softdrink();
            $product->setName($name);
            $product->setStock($stock);
            $product->setPrice($price);
            $product->setDescription($description);

            $em = $this->getDoctrine()->getManager();

            $em->persist($product);
            $em->flush();

            //Get all products from database
            $products = $this->getDoctrine()->getRepository('AppBundle:Softdrink');
            $products = $products->findAll();
            //Render template with all products from database.
            return $this->render('admin.html.twig', array(
                    'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..'),
                    'products' => $products,
                    'info' => $name.' was successfully added!',
                )
            );
        }
    }
}